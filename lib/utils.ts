const checkEmptyProps = (obj: Object): boolean => {
  for (const key in obj) {
    if (obj[key] === '') return true
  }

  return false
}

export { checkEmptyProps }