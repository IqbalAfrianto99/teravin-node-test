import { NextApiHandler } from 'next'
import { query } from '@/lib/db'

const handler: NextApiHandler = async (req, res) => {
  const { q } = req.query
  try {
    const results = await query(`
      SELECT * FROM users
      WHERE name LIKE '%${q}%' OR email LIKE '%${q}%'
      ORDER BY created_at DESC
  `)

    return res.json(results)
  } catch (e) {
    res.status(500).json({ message: e.message })
  }
}

export default handler
