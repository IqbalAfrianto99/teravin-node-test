import { NextApiHandler } from 'next'
import Filter from 'bad-words'
import { query } from '@/lib/db'

const filter = new Filter()

const handler: NextApiHandler = async (req, res) => {
  const { name, email, phone_number, address } = req.body
  try {
    if (!name || !email || !phone_number || !address) {
      return res
        .status(400)
        .json({ message: 'all fields are required' })
    }

    const results = await query(
      `
      INSERT INTO users (name, email, phone_number, address)
      VALUES (?, ?, ?, ?)
      `,
      [
        filter.clean(name), 
        filter.clean(email),
        filter.clean(phone_number),
        filter.clean(address)
      ]
    )

    return res.json(results)
  } catch (e) {
    res.status(500).json({ message: e.message })
  }
}

export default handler
