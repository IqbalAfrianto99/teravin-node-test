import { useEffect, useState } from 'react'
import Skeleton from 'react-loading-skeleton'

import Nav from '@/components/nav'
import Container from '@/components/container'
import Entries from '@/components/entries'

import { useEntries } from '@/lib/swr-hooks'



export default function IndexPage() {
  const { entries, isLoading } = useEntries()
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState(entries)

  useEffect(() => {
    if (!isLoading && entries) {
      setData(entries)
      setLoading(false)
    }

  }, [entries, isLoading])

  const handleSearch = async (q) => {
    let url = `api/users/search?q=${q}`

    if (!q) url = 'api/users'
  
    const res = await window.fetch(url)
    
    if (res.ok) {
      const users = await res.json()

      if (users) setData(users)
    }
  }

  return (
    <>
    {loading ? (
      <div>
        <Nav />
        <Container>
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
          <div className="my-4" />
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
          <div className="my-4" />
          <Skeleton width={180} height={24} />
          <Skeleton height={48} />
        </Container>
      </div>
    ) : (
      <div>
        <Nav />
        <Container>
          <Entries entries={data} onSearch={handleSearch} />
        </Container>
      </div>
    )}
    </>
  )
}
