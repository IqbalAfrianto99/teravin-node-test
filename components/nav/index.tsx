import Link from 'next/link'
import Container from '@/components/container'
import ButtonLink from '@/components/button-link'

interface Props {
  title?: string
  showBtn?: boolean
}

export default function Nav({ title = 'Users', showBtn = true }: Props) {
  return (
    <Container className="py-4">
      <nav>
        <div className="flex justify-between items-center">
          <Link href="/">
            <a className="font-bold text-3xl">{title}</a>
          </Link>
          {showBtn && <ButtonLink href="/new">New User</ButtonLink>}
        </div>
      </nav>
    </Container>
  )
}
