import cn from 'clsx'

interface Props {
  onClick?: any
  className?: string
  children: React.ReactElement | string
  type?: "button" | "submit" | "reset"
  disabled?: boolean
}

function Button({
  onClick,
  className = '',
  children = null,
  type = null,
  disabled = false,
}: Props) {
  return (
    <button
      type={type}
      onClick={onClick}
      disabled={disabled}
      className={cn(
        'bg-blue-600',
        'text-white',
        'p-2',
        'rounded',
        'uppercase',
        'text-sm',
        'font-bold',
        {
          [className]: Boolean(className),
        }
      )}
    >
      {children}
    </button>
  )
}

export default Button
