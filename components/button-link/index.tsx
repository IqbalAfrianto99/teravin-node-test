import Link from 'next/link'
import cn from 'clsx'

interface Props {
  href: string
  className?: string
  children: React.ReactElement | string
}

function ButtonLink({ href = '/', className = '', children }: Props) {
  return (
    <Link href={href}>
      <a
        className={cn(
          'bg-blue-600',
          'text-white',
          'p-2',
          'rounded',
          'uppercase',
          'text-sm',
          'font-bold',
          {
            [className]: Boolean(className),
          }
        )}
      >
        {children}
      </a>
    </Link>
  )
}

export default ButtonLink
