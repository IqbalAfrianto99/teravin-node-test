import { useState } from 'react'
import DataTable from 'react-data-table-component';

import Button from '@/components/button'

const columns = [
  {
    name: 'Name',
    selector: 'name',
    sortable: true
  },
  {
    name: 'Email',
    selector: 'email',
    sortable: true
  }
]

function Entries({ entries, onSearch }) {
    const [search, setSearch] = useState('')

    return (
      <div>
        <div className="flex my-4">
          <input
            id="search"
            className="shadow border rounded h-10 mr-6 p-4"
            type="text"
            name="search"
            placeholder="Search..."
            value={search}
            onKeyDown={(e) => {if (e.key === 'Enter') onSearch(search)}}
            onChange={(e) => setSearch(e.target.value)}
          />
          <Button onClick={() => onSearch(search)} >Search</Button>
        </div>
        {entries?.length > 0 ? (
          <DataTable
            columns={columns}
            data={entries}
          />
        ) : (
          <h3 className="text-center text-lg font-medium my-24">Tidak ada data user, silahkan menambahkan user baru</h3>
        )}
      </div>
    )
}

export default Entries
