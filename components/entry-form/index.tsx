import { useState } from 'react'
import Router from 'next/router'

import Button from '@/components/button'

import { checkEmptyProps } from '@/lib/utils'

export default function EntryForm() {
  const [alert, setAlert] = useState({
    open: false,
    message: ''
  })
  const [adresses, setAdresses] = useState([{ address: '' }])
  const [values, setValues] = useState({
    name: '',
    email: '',
    phone: '',
  })
  const [submitting, setSubmitting] = useState(false)

  const validate = () => {

    const isValuesEmpy = checkEmptyProps(values)
    let isAddressEmpty

    for (const item of adresses) {
      if (item.address === '') isAddressEmpty = true
    }

    if (isAddressEmpty || isValuesEmpy) {
      setAlert({ open: true, message: 'All fields are required' })
      setSubmitting(false)
      return false
    }

    return true
  }

  async function submitHandler(e) {
    setSubmitting(true)
    e.preventDefault()
    
    const valid =  validate()
    if (!valid) return ;

    try {
      const data = {
        name: values.name,
        email: values.email,
        phone_number: values.phone,
        address: JSON.stringify(adresses)
      }

      const res = await fetch('/api/users/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      })
    
      setSubmitting(false)
      const json = await res.json()
    
      if (!res.ok) throw Error(json.message)
    
      Router.push('/')
    
    } catch (e) {
      throw Error(e.message)
    }
  }

  const handleChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value })
  }

  const handleAddressChange = (e, index) => {
    const { name, value } = e.target
    const listAddress = [...adresses]
    listAddress[index][name] = value

    setAdresses(listAddress)
  }

  const handleAdd = (e) => {
    e.preventDefault()
    const listAddress = [...adresses]
    listAddress.push({ address: '' })
    setAdresses(listAddress)
  }

  const handleRemove = (index) => {
    const currentAddresses = [...adresses]
    currentAddresses.splice(index, 1)
    setAdresses(currentAddresses)
  }

  return (
    <form onSubmit={submitHandler}>
      <div className="my-4">
        <label htmlFor="Name">
          <h3 className="font-bold">Name</h3>
        </label>
        <input
          id="name"
          className="shadow border rounded w-full h-8"
          type="text"
          name="name"
          value={values.name}
          onChange={handleChange}
        />
      </div>
      <div className="my-4">
        <label htmlFor="email">
          <h3 className="font-bold">Email</h3>
        </label>
        <input
          id="email"
          className="shadow border rounded w-full h-8"
          type="email"
          name="email"
          value={values.email}
          onChange={handleChange}
        />
      </div>
      <div className="my-4">
        <label htmlFor="phone">
          <h3 className="font-bold">Phone</h3>
        </label>
        <input
          id="phone"
          className="shadow border rounded w-full h-8"
          type="number"
          name="phone"
          value={values.phone}
          onChange={handleChange}
        />
      </div>
      {adresses.map((item, index) => (
        <div key={index} className="my-4">
          <label htmlFor="address">
            <h3 className="font-bold">Address {index > 0 && `#${index+1}`} </h3>
          </label>
          <div className="flex">
            <textarea
              className="shadow border resize-none focus:shadow-outline w-full h-12 mr-8"
              id="address"
              name="address"
              value={item.address}
              onChange={(e) => handleAddressChange(e, index)}
            />
            {index == 0
              ?
              <Button onClick={handleAdd}>Add</Button>
              :
              <Button onClick={() => handleRemove(index)}>X</Button>
            }
          </div>
        </div>
      ))}
      {alert.open && (
        <div className="flex bg-red-200 my-6 br-8 rounded spacex-2">
          <span className="p-2 text-sm text-black opacity-75 font-medium">{alert.message}</span>
          <div className="ml-auto mr-2 pt-1">
            <span className="text-xs"></span>
          </div>
        </div>
      )}
      <Button disabled={submitting} type="submit">
        {submitting ? 'Creating ...' : 'Create'}
      </Button>
      <Button onClick={() => Router.push('/')} className="mx-6">
        Back
      </Button>
    </form>
  )
}
